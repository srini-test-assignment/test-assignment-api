package ee.srini.configuration.clients;

import ee.srini.adapters.web.client.json.CreateClientInputJson;
import ee.srini.adapters.web.client.json.UpdateClientInputJson;

public class TestData {

    public static final long ID = -2L;
    public static final long USER_ID = -1L;
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email@gmail.com";
    public static final String ADDRESS = "address";
    public static final long COUNTRY_ID = -100L;

    public static CreateClientInputJson createClientInputJsonWithInvalidEmail() {
        return new CreateClientInputJson(null, FIRST_NAME, LAST_NAME, USERNAME, "invalid.email.pattern", ADDRESS, COUNTRY_ID);
    }

    public static CreateClientInputJson createClientInputJsonWithInvalidUsername() {
        return new CreateClientInputJson(null, FIRST_NAME, LAST_NAME, null, EMAIL, ADDRESS, COUNTRY_ID);
    }

    public static CreateClientInputJson createInvalidClientInputJson() {
        return new CreateClientInputJson(null, FIRST_NAME, null, null, "invalid.email.pattern", ADDRESS, COUNTRY_ID);
    }

    public static UpdateClientInputJson validUpdateClientInputJson() {
        return new UpdateClientInputJson(ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }
}
