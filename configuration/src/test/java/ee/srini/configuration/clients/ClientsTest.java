package ee.srini.configuration.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.srini.TestAssignmentApiApplication;
import ee.srini.adapters.web.authentication.json.LoginRequestJson;
import ee.srini.adapters.web.authentication.json.LoginResponseJson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ContextConfiguration(classes = TestAssignmentApiApplication.class)
@TestPropertySource(locations = "/test-application.yml")
public class ClientsTest {

    @Autowired
    private MockMvc mvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void createClient_withInvalidEmail_shouldThrowError() throws Exception {
        var clientRequest = TestData.createClientInputJsonWithInvalidEmail();
        mvc.perform(authorizedPost("/api/client", clientRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is("Invalid email pattern")));
    }

    @Test
    public void createClient_withInvalidUsername_shouldThrowError() throws Exception {
        var clientRequest = TestData.createClientInputJsonWithInvalidUsername();
        mvc.perform(authorizedPost("/api/client", clientRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is("must not be blank")));
    }

    @Test
    public void createClient_withMultipleIncorrectFields_shouldThrowError() throws Exception {
        var clientRequest = TestData.createInvalidClientInputJson();
        mvc.perform(authorizedPost("/api/client", clientRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void createClient_withInvalidBody_shouldThrowError() throws Exception {
        var clientRequest = TestData.validUpdateClientInputJson();
        mvc.perform(authorizedPost("/api/client", clientRequest))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is("must be null")));
    }

    private MockHttpServletRequestBuilder authorizedPost(String url, Object body) throws Exception {
        String jwt = performLogin();
        return post(url)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwt)
                .content(objectMapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON);
    }

    private String performLogin() throws Exception {
        LoginRequestJson loginRequestJson = new LoginRequestJson("john", "johnpass");
        MvcResult loginResult = mvc.perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequestJson)))
                .andExpect(status().isOk())
                .andReturn();
        LoginResponseJson response = objectMapper.readValue(loginResult.getResponse().getContentAsString(), LoginResponseJson.class);
        return response.jwt();
    }

}
