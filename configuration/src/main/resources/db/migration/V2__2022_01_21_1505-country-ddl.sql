CREATE TABLE country
(
    id   IDENTITY     PRIMARY KEY,
    name VARCHAR(20)  NOT NULL
);