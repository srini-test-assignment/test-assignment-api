//john - johnpass
INSERT INTO login_user (id, username, password) VALUES (1, 'john', '$2a$10$Yanb4osAxPkvFF2zy9ReE.Fen9QzX2dVEy61479qcM92AjcEFnHqi');
//jane - janepass
INSERT INTO login_user (username, password) VALUES ('jane', '$2a$10$LCme4mnVp8xO7diyFhCl3eN9j1ZkAU17Km9lAFY6HspOf5OdIlHYK');
//jack - jackpass
INSERT INTO login_user (username, password) VALUES ('jack', '$2a$10$QlHZa2jr6g/aKaKAS9QxAO5ktnmmFe38rAjZFzMI0KwSfJ7aZ7Sty');