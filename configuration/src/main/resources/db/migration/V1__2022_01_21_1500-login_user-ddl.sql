CREATE TABLE login_user
(
    id       IDENTITY     PRIMARY KEY,
    username VARCHAR(30)  NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);