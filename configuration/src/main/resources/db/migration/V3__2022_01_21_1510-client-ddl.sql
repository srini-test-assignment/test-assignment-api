CREATE TABLE client
(
    id          IDENTITY        PRIMARY KEY,
    user_id     BIGINT          NOT NULL,
    first_name  VARCHAR(30)     NOT NULL,
    last_name   VARCHAR(30)     NOT NULL,
    username    VARCHAR(30)     NOT NULL UNIQUE,
    email       VARCHAR(255)    UNIQUE,
    address     VARCHAR(255),
    country_id  BIGINT
);

ALTER TABLE client
    ADD FOREIGN KEY (user_id)
        REFERENCES login_user(id);

ALTER TABLE client
    ADD FOREIGN KEY (country_id)
        REFERENCES country(id);
