plugins {
    id("org.springframework.boot")
    id("org.flywaydb.flyway")
}

dependencies {

    // inner application dependies
    implementation(project(":common"))
    implementation(project(":application"))
    implementation(project(":adapters:persistence"))
    implementation(project(":adapters:web"))

    // spring
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    //database
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.6.2")
    implementation("com.h2database:h2:1.4.200")
    implementation("org.flywaydb:flyway-core")


    // testing
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")



}
repositories {
    mavenCentral()
}

flyway {
    url = "jdbc:h2:file:~/test"
    user = "sa"
    password = ""
}