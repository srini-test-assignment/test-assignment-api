package ee.srini.application.usecase.client;

import ee.srini.application.domain.client.Client;
import ee.srini.common.annotation.UseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetClients {

    private final ClientGateway clientGateway;

    public Response execute(Request request) {
        var clients = clientGateway.findAllByUserId(request.userId);
        return new Response(clients);
    }

    public record Request(long userId) {}
    public record Response(List<Client> clients) {}

}
