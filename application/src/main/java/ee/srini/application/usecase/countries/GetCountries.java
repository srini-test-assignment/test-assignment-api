package ee.srini.application.usecase.countries;

import ee.srini.application.domain.countries.Country;
import ee.srini.common.annotation.UseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetCountries {

    private final CountryGateway countryGateway;

    public Response execute() {
        return new Response(countryGateway.getCountries());
    }

    public record Response(List<Country> country) {}

}
