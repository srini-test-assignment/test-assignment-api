package ee.srini.application.usecase.authentication;

import ee.srini.application.domain.authentication.LoginUser;

import java.util.Optional;

public interface LoginUserGateway {
    Optional<LoginUser> findByUsername(String username);
}
