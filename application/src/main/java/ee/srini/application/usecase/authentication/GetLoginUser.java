package ee.srini.application.usecase.authentication;

import ee.srini.application.domain.authentication.LoginUser;
import ee.srini.common.annotation.UseCase;
import ee.srini.common.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class GetLoginUser {

    private final LoginUserGateway loginUserGateway;

    public Response execute(Request request) {
        LoginUser user = loginUserGateway.findByUsername(request.username).orElseThrow(UserNotFoundException::new);
        return new Response(user);
    }

    public record Request(String username) {}

    public record Response(LoginUser loginUser) {}
}
