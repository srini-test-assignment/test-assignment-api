package ee.srini.application.usecase.client;

import ee.srini.application.domain.client.Client;
import ee.srini.common.annotation.UseCase;
import ee.srini.common.exception.ClientNotFoundException;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class GetClient {

    private final ClientGateway clientGateway;

    public Response execute(Request request) {
        var client = clientGateway.getByIdAndUserId(request.id, request.userId)
                .orElseThrow(ClientNotFoundException::new);
        return new Response(client);
    }

    public record Request(long id, long userId) {}

    public record Response(Client client) {}
}
