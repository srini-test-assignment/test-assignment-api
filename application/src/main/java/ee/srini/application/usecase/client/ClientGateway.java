package ee.srini.application.usecase.client;

import ee.srini.application.domain.client.Client;

import java.util.List;
import java.util.Optional;

public interface ClientGateway {

    Optional<Client> getByIdAndUserId(long id, long userId);

    List<Client> findAllByUserId(long userId);

    void save(Client client);
}
