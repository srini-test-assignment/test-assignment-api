package ee.srini.application.usecase.client;

import ee.srini.application.domain.client.Client;
import ee.srini.common.annotation.UseCase;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class SaveClient {

    private final ClientGateway clientGateway;

    public void execute(Request request) {
        clientGateway.save(request.client);
    }

    public record Request(Client client) {}

}
