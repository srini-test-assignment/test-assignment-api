package ee.srini.application.usecase.countries;

import ee.srini.application.domain.countries.Country;

import java.util.List;

public interface CountryGateway {

    List<Country> getCountries();
}
