package ee.srini.application.domain.client;

public record Client(
        Long id,
        long userId,
        String firstName,
        String lastName,
        String username,
        String email,
        String address,
        long countryId
) {}
