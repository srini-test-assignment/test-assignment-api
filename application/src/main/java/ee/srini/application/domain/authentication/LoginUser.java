package ee.srini.application.domain.authentication;

public record LoginUser(
        long id,
        String username,
        String password) {
}
