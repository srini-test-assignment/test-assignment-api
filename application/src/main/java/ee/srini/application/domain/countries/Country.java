package ee.srini.application.domain.countries;

public record Country(long id, String name) { }
