package ee.srini.application.usecase;

import ee.srini.application.usecase.client.ClientGateway;
import ee.srini.application.usecase.client.SaveClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static ee.srini.application.usecase.TestData.getMockClient;
import static org.mockito.Mockito.*;

public class SaveClientTest {

    private ClientGateway gateway;
    private SaveClient saveClient;

    @BeforeEach
    void setup() {
        gateway = mock(ClientGateway.class);
        saveClient = new SaveClient(gateway);
    }

    @Test
    void execute_withValidRequest_shouldRespond() {
        var expectedClient = getMockClient();
        doNothing().when(gateway).save(expectedClient);

        saveClient.execute(new SaveClient.Request(getMockClient()));

        verify(gateway).save(expectedClient);
    }
}
