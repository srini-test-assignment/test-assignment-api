package ee.srini.application.usecase;

import ee.srini.application.usecase.client.ClientGateway;
import ee.srini.application.usecase.client.GetClients;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static ee.srini.application.usecase.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetClientsTest {

    private ClientGateway gateway;
    private GetClients getClients;

    @BeforeEach
    void setup() {
        gateway = mock(ClientGateway.class);
        getClients = new GetClients(gateway);
    }

    @Test
    void execute_withValidRequest_shouldRespond() {
        when(gateway.findAllByUserId(USER_ID)).thenReturn(List.of(getMockClient()));

        var response = getClients.execute(new GetClients.Request(USER_ID));

        assertThat(response.clients()).singleElement().satisfies(client -> {
            assertThat(client.id()).isEqualTo(ID);
            assertThat(client.userId()).isEqualTo(USER_ID);
            assertThat(client.firstName()).isEqualTo(FIRST_NAME);
        });
    }

    @Test
    void execute_withInvalidUserId_shouldReturnEmptyList() {
        when(gateway.findAllByUserId(USER_ID)).thenReturn(Collections.emptyList());

        var response = getClients.execute(new GetClients.Request(USER_ID));

        assertThat(response.clients()).isEmpty();
    }
}
