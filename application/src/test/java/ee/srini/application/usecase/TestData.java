package ee.srini.application.usecase;

import ee.srini.application.domain.client.Client;

import java.util.Optional;

public class TestData {

    public static final long ID = -2L;
    public static final long USER_ID = -1L;
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final long COUNTRY_ID = -100L;

    public static Optional<Client> mockGetClientResponse() {
        return Optional.of(getMockClient());
    }

    public static Client getMockClient() {
        return new Client(ID, USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }
}
