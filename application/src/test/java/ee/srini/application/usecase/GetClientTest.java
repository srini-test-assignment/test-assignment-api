package ee.srini.application.usecase;

import ee.srini.application.usecase.client.ClientGateway;
import ee.srini.application.usecase.client.GetClient;
import ee.srini.common.exception.ClientNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static ee.srini.application.usecase.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetClientTest {

    private ClientGateway gateway;
    private GetClient getClient;

    @BeforeEach
    void setup() {
        gateway = mock(ClientGateway.class);
        getClient = new GetClient(gateway);
    }

    @Test
    void execute_withValidRequest_shouldRespond() {
        when(gateway.getByIdAndUserId(ID, USER_ID)).thenReturn(TestData.mockGetClientResponse());

        var response = getClient.execute(new GetClient.Request(ID, USER_ID));

        assertThat(response.client()).satisfies(client -> {
            assertThat(client.id()).isEqualTo(ID);
            assertThat(client.userId()).isEqualTo(USER_ID);
            assertThat(client.firstName()).isEqualTo(FIRST_NAME);
        });
    }

    @Test
    void execute_withInvalidUserId_shouldThrowException() {
        when(gateway.getByIdAndUserId(ID, USER_ID)).thenReturn(Optional.empty());
        assertThrows(ClientNotFoundException.class, () -> getClient.execute(new GetClient.Request(ID, USER_ID)));
    }

}
