# test-assignment-api

Test-assignment-api is the backend application for adding and managing clients for users.

# Project structure
Application is built using hexagonal architecture and consists of 4 main parts
```
common/                 common sharable code, no external frameworks!
adapters/web            adapters for client to communicate with the api
adapters/persistence    adapters for communication with the database or other external resources
application/            core business logic
configuration/          spring boot module and all configurations
```

### Development environment setup
* JDK17 and JAVA_HOME set
* IDEA plugins: Lombok
* Enable -> File | Settings | Build, Execution, Deployment | Compiler | Annotation Processors
* Run the application as any spring boot app by starting the main method of the application: configuration/src/main/java/ee/srini/TestAssignmentApiApplication.java
* To authenticate and receive jwt create post request to url localhost:8080/test-assignment/api/auth/login with any of the user credentials as request body:
```
john johnpass
jane janepass   
jack jackpass
```
* Example:
```
post http://localhost:8080/test-assignment/api/auth/login
{
    "username": "john",
    "password": "johnpass"
}
```
