package ee.srini.adapters.web.client.json;

public record ClientListViewJson(
        long id,
        String firstName,
        String lastName,
        String username
) { }
