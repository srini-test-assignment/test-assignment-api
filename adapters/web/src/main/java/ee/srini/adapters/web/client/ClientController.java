package ee.srini.adapters.web.client;

import ee.srini.adapters.web.client.json.ClientJson;
import ee.srini.adapters.web.client.json.ClientListViewJson;
import ee.srini.adapters.web.client.json.CreateClientInputJson;
import ee.srini.adapters.web.client.json.UpdateClientInputJson;
import ee.srini.adapters.web.security.config.SriniUser;
import ee.srini.application.usecase.client.GetClient;
import ee.srini.application.usecase.client.GetClients;
import ee.srini.application.usecase.client.SaveClient;
import ee.srini.common.annotation.WebAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@WebAdapter
@RestController
@RequestMapping("api/client")
@RequiredArgsConstructor
public class ClientController {

    private final GetClient getClient;
    private final GetClients getClients;
    private final SaveClient saveClient;

    @GetMapping("/{id}")
    public ResponseEntity<ClientJson> getClient(@PathVariable long id) {
        SriniUser user = (SriniUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var respone = getClient.execute(new GetClient.Request(id, user.getUserId()));

        var presenter = new ClientPresenter();
        presenter.present(respone);
        return presenter.getViewModel();

    }

    @GetMapping
    public ResponseEntity<List<ClientListViewJson>> getUserClients() {
        SriniUser user = (SriniUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var response = getClients.execute(new GetClients.Request(user.getUserId()));

        var presenter = new ClientListViewPresenter();
        presenter.present(response);
        return presenter.getViewModel();
    }

    @PostMapping
    public ResponseEntity<Void> createClient(@Valid @RequestBody CreateClientInputJson input) {
        SriniUser user = (SriniUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        saveClient.execute(input.toRequest(user.getUserId()));

        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateClient(@PathVariable long id, @Valid @RequestBody UpdateClientInputJson input) {
        SriniUser user = (SriniUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        saveClient.execute(input.toRequest(user.getUserId()));

        return ResponseEntity.ok().build();
    }

}
