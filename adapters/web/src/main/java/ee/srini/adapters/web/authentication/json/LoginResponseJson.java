package ee.srini.adapters.web.authentication.json;

public record LoginResponseJson(String jwt) {
}
