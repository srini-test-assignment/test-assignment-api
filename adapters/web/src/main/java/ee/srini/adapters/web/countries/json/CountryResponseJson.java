package ee.srini.adapters.web.countries.json;

public record CountryResponseJson(long id, String name) { }
