package ee.srini.adapters.web.client.json;

public record ClientJson(
        Long id,
        long userId,
        String firstName,
        String lastName,
        String username,
        String email,
        String address,
        long countryId
) {}
