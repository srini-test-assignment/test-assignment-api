package ee.srini.adapters.web.security.jwt;

import ee.srini.adapters.web.exception.JwtException;
import ee.srini.adapters.web.security.config.SriniUser;
import ee.srini.adapters.web.security.config.UserLoginDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {

    private final UserLoginDetailsService userLoginDetailsService;
    private final JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = request.getHeader("Authorization");

        if (isAuthorizationHeaderValidBearerToken(authorizationHeader)) {
            String jwt = jwtUtil.extractJwtFromBearer(authorizationHeader);
            String username = jwtUtil.extractUsername(jwt);

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                SriniUser userDetails = userLoginDetailsService.loadUserByUsername(username);
                if (jwtUtil.isValidJwt(jwt, userDetails)) {
                    var token = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    token.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(token);
                } else {
                    throw new JwtException();
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    private boolean isAuthorizationHeaderValidBearerToken(String authorizationHeader) {
        return authorizationHeader != null && authorizationHeader.startsWith("Bearer ");
    }
}
