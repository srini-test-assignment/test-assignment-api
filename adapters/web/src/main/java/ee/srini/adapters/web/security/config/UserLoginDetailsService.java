package ee.srini.adapters.web.security.config;

import ee.srini.application.domain.authentication.LoginUser;
import ee.srini.application.usecase.authentication.GetLoginUser;
import ee.srini.application.usecase.authentication.GetLoginUser.Request;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserLoginDetailsService implements UserDetailsService {

    private final GetLoginUser getLoginUser;

    @Override
    public SriniUser loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = getLoginUser.execute(new Request(username)).loginUser();
        return new SriniUser(loginUser.id(), loginUser.username(), loginUser.password(), Collections.emptyList());
    }
}
