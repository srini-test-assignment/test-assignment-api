package ee.srini.adapters.web.authentication;

import ee.srini.adapters.web.authentication.json.LoginResponseJson;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

public class AuthenticationResponsePresenter {

    private LoginResponseJson result;

    public void present(String jwt) {
        this.result = new LoginResponseJson(jwt);
    }

    public ResponseEntity<LoginResponseJson> getViewModel() {
        Assert.notNull(result, () -> "Login Response must be presented");
        return ResponseEntity.ok(result);
    }
}
