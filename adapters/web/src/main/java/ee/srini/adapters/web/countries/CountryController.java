package ee.srini.adapters.web.countries;

import ee.srini.adapters.web.countries.json.CountryResponseJson;
import ee.srini.application.usecase.countries.GetCountries;
import ee.srini.common.annotation.WebAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@WebAdapter
@RestController
@RequestMapping("api/country")
@RequiredArgsConstructor
public class CountryController {

    private final GetCountries getCountries;

    @GetMapping
    public ResponseEntity<List<CountryResponseJson>> getCountries() {
        var response = getCountries.execute();
        var presenter = new CountryPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
