package ee.srini.adapters.web.authentication;

import ee.srini.adapters.web.authentication.json.LoginRequestJson;
import ee.srini.adapters.web.authentication.json.LoginResponseJson;
import ee.srini.common.annotation.WebAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationInteractor authenticationInteractor;

    @PostMapping("/login")
    public ResponseEntity<LoginResponseJson> login(@RequestBody LoginRequestJson request) {
        var jwt = authenticationInteractor.generateJwt(request);
        var presenter = new AuthenticationResponsePresenter();

        presenter.present(jwt);
        return presenter.getViewModel();
    }


}
