package ee.srini.adapters.web.exception;

public class JwtException extends RuntimeException {

    public JwtException() {
        super("jwt incorrect");
    }
}
