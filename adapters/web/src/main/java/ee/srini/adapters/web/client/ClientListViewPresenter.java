package ee.srini.adapters.web.client;

import ee.srini.adapters.web.client.json.ClientListViewJson;
import ee.srini.application.domain.client.Client;
import ee.srini.application.usecase.client.GetClients.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

public class ClientListViewPresenter {

    private List<ClientListViewJson> result;

    public void present(Response response) {
        result = response.clients().stream().map(this::toClientListViewJson).toList();
    }

    private ClientListViewJson toClientListViewJson(Client client) {
        return new ClientListViewJson(
                client.id(),
                client.firstName(),
                client.lastName(),
                client.username()
        );
    }

    public ResponseEntity<List<ClientListViewJson>> getViewModel() {
        Assert.notNull(result, () -> "Countries must be presented");
        return ResponseEntity.ok(result);
    }

}
