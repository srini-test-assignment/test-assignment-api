package ee.srini.adapters.web.authentication;

import ee.srini.adapters.web.authentication.json.LoginRequestJson;
import ee.srini.adapters.web.security.config.SriniUser;
import ee.srini.adapters.web.security.jwt.JwtUtil;
import ee.srini.common.annotation.WebAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;

@WebAdapter
@RequiredArgsConstructor
public class AuthenticationInteractor {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;

    public String generateJwt(LoginRequestJson request) {
        authenticationManager.authenticate(request.toUsernamePasswordAuthenticationToken());
        SriniUser useDetails = (SriniUser) userDetailsService.loadUserByUsername(request.username());
        return jwtUtil.generateJwt(useDetails);
    }

}
