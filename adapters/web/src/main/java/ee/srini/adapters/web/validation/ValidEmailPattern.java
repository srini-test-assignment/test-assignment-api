package ee.srini.adapters.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EmailPatternValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEmailPattern {
    String message() default "Invalid email pattern";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
