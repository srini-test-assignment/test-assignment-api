package ee.srini.adapters.web.authentication.json;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public record LoginRequestJson(String username, String password) {

    public UsernamePasswordAuthenticationToken toUsernamePasswordAuthenticationToken() {
        return new UsernamePasswordAuthenticationToken(this.username, this.password);
    }
}
