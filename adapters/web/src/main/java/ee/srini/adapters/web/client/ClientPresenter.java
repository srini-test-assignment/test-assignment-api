package ee.srini.adapters.web.client;

import ee.srini.adapters.web.client.json.ClientJson;
import ee.srini.application.domain.client.Client;
import ee.srini.application.usecase.client.GetClient.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

public class ClientPresenter {

    private ClientJson result;

    public void present(Response response) {
        result = toClientJson(response.client());
    }

    private ClientJson toClientJson(Client client) {
        return new ClientJson(
                client.id(),
                client.userId(),
                client.firstName(),
                client.lastName(),
                client.username(),
                client.email(),
                client.address(),
                client.countryId()
        );
    }

    public ResponseEntity<ClientJson> getViewModel() {
        Assert.notNull(result, () -> "Countries must be presented");
        return ResponseEntity.ok(result);
    }
}
