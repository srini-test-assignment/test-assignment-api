package ee.srini.adapters.web.countries;

import ee.srini.adapters.web.countries.json.CountryResponseJson;
import ee.srini.application.domain.countries.Country;
import ee.srini.application.usecase.countries.GetCountries.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

public class CountryPresenter {

    private List<CountryResponseJson> result;

    public void present(Response response) {
        result = response.country().stream().map(this::toCountryJson).toList();
    }

    private CountryResponseJson toCountryJson(Country country) {
        return new CountryResponseJson(country.id(), country.name());
    }

    public ResponseEntity<List<CountryResponseJson>> getViewModel() {
        Assert.notNull(result, () -> "Countries must be presented");
        return ResponseEntity.ok(result);
    }
}
