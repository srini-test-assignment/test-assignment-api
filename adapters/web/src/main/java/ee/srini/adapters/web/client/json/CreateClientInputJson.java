package ee.srini.adapters.web.client.json;

import ee.srini.adapters.web.validation.ValidEmailPattern;
import ee.srini.application.domain.client.Client;
import ee.srini.application.usecase.client.SaveClient;
import ee.srini.application.usecase.client.SaveClient.Request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public record CreateClientInputJson(
        @Null Long id,
        @NotBlank String firstName,
        @NotBlank String lastName,
        @NotBlank String username,
        @ValidEmailPattern String email,
        String address,
        @NotNull long countryId) {


    public Request toRequest(long userId) {
        return new SaveClient.Request(new Client(
                null,
                userId,
                firstName,
                lastName,
                username,
                email,
                address,
                countryId));
    }
}
