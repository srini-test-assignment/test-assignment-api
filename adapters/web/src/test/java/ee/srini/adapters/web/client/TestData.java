package ee.srini.adapters.web.client;

import ee.srini.adapters.web.client.json.CreateClientInputJson;
import ee.srini.adapters.web.client.json.UpdateClientInputJson;
import ee.srini.application.domain.client.Client;
import ee.srini.application.usecase.client.GetClient;
import ee.srini.application.usecase.client.GetClients;
import ee.srini.application.usecase.client.SaveClient;

import java.util.List;

public class TestData {

    public static final long ID = -2L;
    public static final long USER_ID = -1L;
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final long COUNTRY_ID = -100L;

    public static GetClient.Response getClientMockResponse() {
        return new GetClient.Response(getMockClient());
    }

    public static GetClients.Response getClientsMockResponse() {
        return new GetClients.Response(List.of(getMockClient()));
    }

    public static SaveClient.Request saveNewClientMockRequest() {
        return new SaveClient.Request(getNewMockClient());
    }

    public static SaveClient.Request saveExistingClientMockRequest() {
        return new SaveClient.Request(getMockClient());
    }

    public static Client getMockClient() {
        return new Client(ID, USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }

    public static Client getNewMockClient() {
        return new Client(null, USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }

    public static CreateClientInputJson validCreateClientInputJson() {
        return new CreateClientInputJson(null, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }

    public static UpdateClientInputJson validUpdateClientInputJson() {
        return new UpdateClientInputJson(ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }
}
