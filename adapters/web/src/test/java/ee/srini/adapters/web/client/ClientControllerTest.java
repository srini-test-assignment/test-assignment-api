package ee.srini.adapters.web.client;

import ee.srini.adapters.web.client.json.ClientJson;
import ee.srini.adapters.web.client.json.ClientListViewJson;
import ee.srini.adapters.web.common.BaseControllerTest;
import ee.srini.application.usecase.client.GetClient;
import ee.srini.application.usecase.client.GetClients;
import ee.srini.application.usecase.client.SaveClient;
import ee.srini.common.exception.ClientNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ClientControllerTest extends BaseControllerTest {

    private GetClient getClient;
    private GetClients getClients;
    private SaveClient saveClient;
    private ClientController controller;

    @BeforeEach
    void setup() {
        mockAuthentication();
        getClient = mock(GetClient.class);
        getClients = mock(GetClients.class);
        saveClient = mock(SaveClient.class);
        controller = new ClientController(getClient, getClients, saveClient);
    }

    @Test
    void getClient_withValidInput_shouldReturnClient() {
        GetClient.Request expectedRequest = new GetClient.Request(1L, MOCK_USER_ID);
        when(getClient.execute(expectedRequest)).thenReturn(TestData.getClientMockResponse());

        var response = controller.getClient(1L);

        assertThat(Objects.requireNonNull(response.getBody())).satisfies(this::verifyClientJson);
    }

    @Test
    void getClient_withInvlidInput_shouldThrowException() {
        GetClient.Request expectedRequest = new GetClient.Request(1L, MOCK_USER_ID);
        when(getClient.execute(expectedRequest)).thenThrow(new ClientNotFoundException());

        assertThrows(ClientNotFoundException.class, () -> controller.getClient(1L));
    }

    @Test
    void getClients_withValidInput_shouldReturnClient() {
        GetClients.Request expectedRequest = new GetClients.Request(MOCK_USER_ID);
        when(getClients.execute(expectedRequest)).thenReturn(TestData.getClientsMockResponse());

        var response = controller.getUserClients();

        assertThat(Objects.requireNonNull(response.getBody())).singleElement().satisfies(this::verifyClientJson);
    }

    @Test
    void getClients_withUserWithoutClients_shouldReturnEmptyList() {
        GetClients.Request expectedRequest = new GetClients.Request(MOCK_USER_ID);
        when(getClients.execute(expectedRequest)).thenReturn(new GetClients.Response(Collections.emptyList()));

        var response = controller.getUserClients();

        assertThat(Objects.requireNonNull(response.getBody())).hasSize(0);
    }

    @Test
    void saveClient_withValidInput_shouldCallUseCase() {
        SaveClient.Request expectedRequest = TestData.saveNewClientMockRequest();
        doNothing().when(saveClient).execute(expectedRequest);

        controller.createClient(TestData.validCreateClientInputJson());

        verify(saveClient).execute(expectedRequest);
    }

    @Test
    void updateClient_withValidInput_shouldCallUseCase() {
        SaveClient.Request expectedRequest = TestData.saveExistingClientMockRequest();
        doNothing().when(saveClient).execute(expectedRequest);

        controller.updateClient(TestData.ID, TestData.validUpdateClientInputJson());

        verify(saveClient).execute(expectedRequest);
    }

    private void verifyClientJson(ClientListViewJson clientListViewJson) {
        assertThat(clientListViewJson.id()).isEqualTo(TestData.ID);
        assertThat(clientListViewJson.firstName()).isEqualTo(TestData.FIRST_NAME);
        assertThat(clientListViewJson.lastName()).isEqualTo(TestData.LAST_NAME);
        assertThat(clientListViewJson.username()).isEqualTo(TestData.USERNAME);
    }

    private void verifyClientJson(ClientJson clientJson) {
        assertThat(clientJson.id()).isEqualTo(TestData.ID);
        assertThat(clientJson.userId()).isEqualTo(TestData.USER_ID);
        assertThat(clientJson.firstName()).isEqualTo(TestData.FIRST_NAME);
        assertThat(clientJson.lastName()).isEqualTo(TestData.LAST_NAME);
        assertThat(clientJson.username()).isEqualTo(TestData.USERNAME);
        assertThat(clientJson.email()).isEqualTo(TestData.EMAIL);
        assertThat(clientJson.address()).isEqualTo(TestData.ADDRESS);
        assertThat(clientJson.countryId()).isEqualTo(TestData.COUNTRY_ID);
    }
}
