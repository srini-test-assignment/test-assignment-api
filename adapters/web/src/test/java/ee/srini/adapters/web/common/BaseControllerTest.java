package ee.srini.adapters.web.common;

import ee.srini.adapters.web.security.config.SriniUser;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseControllerTest {

    public static final long MOCK_USER_ID = -1L;
    public static final String MOCK_USERNAME = "mockuser";
    public static final String MOCK_PASSWORD = "mockpass";

    protected void mockAuthentication() {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(new SriniUser(MOCK_USER_ID, MOCK_USERNAME, MOCK_PASSWORD, Collections.emptyList()));
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

}
