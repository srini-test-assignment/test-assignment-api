package ee.srini.adapters.persistence.client;

import ee.srini.adapters.persistence.client.entity.ClientEntity;
import ee.srini.application.domain.client.Client;

import java.util.List;

public class TestData {

    public static final long ID = -2L;
    public static final long USER_ID = -1L;
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final long COUNTRY_ID = -100L;

    public static List<ClientEntity> createMockClientEntityList() {
        return List.of(createMockClientEntity());
    }

    public static ClientEntity createMockClientEntity() {
        return ClientEntity.builder()
                .id(ID)
                .userId(USER_ID)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .username(USERNAME)
                .email(EMAIL)
                .address(ADDRESS)
                .countryId(COUNTRY_ID)
                .build();
    }

    public static Client getMockClient() {
        return new Client(ID, USER_ID, FIRST_NAME, LAST_NAME, USERNAME, EMAIL, ADDRESS, COUNTRY_ID);
    }
}
