package ee.srini.adapters.persistence.client;

import ee.srini.adapters.persistence.client.entity.Clients;
import ee.srini.application.domain.client.Client;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static ee.srini.adapters.persistence.client.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientAdapterTest {

    private Clients clients;
    private ClientAdapter clientAdapter;

    @BeforeEach
    void setup() {
        clients = mock(Clients.class);
        clientAdapter = new ClientAdapter(clients);
    }

    @Test
    void getByIdAndUserId_withValidInput_shouldMapAndRespond() {
        when(clients.findByIdAndUserId(ID, USER_ID))
                .thenReturn(Optional.of(TestData.createMockClientEntity()));

        var response = clientAdapter.getByIdAndUserId(ID, USER_ID);

        assertThat(response).hasValueSatisfying(this::validateClient);
    }

    @Test
    void findAllByUserId_withValidInput_shouldMapAndRespond() {
        when(clients.findAllByUserId(USER_ID)).thenReturn(createMockClientEntityList());

        var response = clientAdapter.findAllByUserId(USER_ID);

        assertThat(response).singleElement().satisfies(this::validateClient);
    }

    private void validateClient(Client client) {
        assertThat(client.id()).isEqualTo(ID);
        assertThat(client.userId()).isEqualTo(USER_ID);
        assertThat(client.firstName()).isEqualTo(FIRST_NAME);
        assertThat(client.lastName()).isEqualTo(LAST_NAME);
        assertThat(client.username()).isEqualTo(USERNAME);
        assertThat(client.email()).isEqualTo(EMAIL);
        assertThat(client.address()).isEqualTo(ADDRESS);
        assertThat(client.countryId()).isEqualTo(COUNTRY_ID);
    }

}
