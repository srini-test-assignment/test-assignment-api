package ee.srini.adapters.persistence.client.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface Clients extends JpaRepository<ClientEntity, Long> {

    List<ClientEntity> findAllByUserId(long userId);

    Optional<ClientEntity> findByIdAndUserId(long id, long userId);
}
