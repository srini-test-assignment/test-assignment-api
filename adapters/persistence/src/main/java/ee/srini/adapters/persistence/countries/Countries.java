package ee.srini.adapters.persistence.countries;

import ee.srini.adapters.persistence.countries.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Countries extends JpaRepository<CountryEntity, Long> {

    List<CountryEntity> findAll();
}
