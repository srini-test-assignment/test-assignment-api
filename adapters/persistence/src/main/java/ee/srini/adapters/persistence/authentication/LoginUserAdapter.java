package ee.srini.adapters.persistence.authentication;

import ee.srini.adapters.persistence.authentication.entity.LoginUserEntity;
import ee.srini.application.domain.authentication.LoginUser;
import ee.srini.application.usecase.authentication.LoginUserGateway;
import ee.srini.common.annotation.PersistenceAdapter;

import java.util.Optional;

@PersistenceAdapter
public class LoginUserAdapter implements LoginUserGateway {

    private final LoginUsers loginUsers;

    public LoginUserAdapter(LoginUsers loginUsers) {
        this.loginUsers = loginUsers;
    }

    @Override
    public Optional<LoginUser> findByUsername(String username) {
        return loginUsers.findByUsername(username).map(this::toLoginUser);
    }

    private LoginUser toLoginUser(LoginUserEntity loginUserEntity) {
        return new LoginUser(
                loginUserEntity.id(),
                loginUserEntity.username(),
                loginUserEntity.password());
    }
}
