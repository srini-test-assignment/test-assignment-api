package ee.srini.adapters.persistence.client;

import ee.srini.adapters.persistence.client.entity.ClientEntity;
import ee.srini.adapters.persistence.client.entity.Clients;
import ee.srini.application.domain.client.Client;
import ee.srini.application.usecase.client.ClientGateway;
import ee.srini.common.annotation.PersistenceAdapter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class ClientAdapter implements ClientGateway {

    private final Clients clients;

    @Override
    public Optional<Client> getByIdAndUserId(long id, long userId) {
        return clients.findByIdAndUserId(id, userId).map(this::toClient);
    }

    @Override
    public List<Client> findAllByUserId(long userId) {
        return clients.findAllByUserId(userId).stream().map(this::toClient).toList();
    }

    @Override
    public void save(Client client) {
        clients.save(toClientEntity(client));
    }

    private Client toClient(ClientEntity clientEntity) {
        return new Client(
                clientEntity.id(),
                clientEntity.userId(),
                clientEntity.firstName(),
                clientEntity.lastName(),
                clientEntity.username(),
                clientEntity.email(),
                clientEntity.address(),
                clientEntity.countryId()
        );
    }

    private ClientEntity toClientEntity(Client client) {
        return ClientEntity.builder()
                .id(client.id())
                .userId(client.userId())
                .firstName(client.firstName())
                .lastName(client.lastName())
                .username(client.username())
                .email(client.email())
                .address(client.address())
                .countryId(client.countryId())
                .build();
    }
}
