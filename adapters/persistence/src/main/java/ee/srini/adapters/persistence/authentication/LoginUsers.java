package ee.srini.adapters.persistence.authentication;

import ee.srini.adapters.persistence.authentication.entity.LoginUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginUsers extends JpaRepository<LoginUserEntity, Long> {

    Optional<LoginUserEntity> findByUsername(String username);
}
