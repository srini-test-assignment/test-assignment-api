package ee.srini.adapters.persistence.countries;

import ee.srini.adapters.persistence.countries.entity.CountryEntity;
import ee.srini.application.domain.countries.Country;
import ee.srini.application.usecase.countries.CountryGateway;
import ee.srini.common.annotation.PersistenceAdapter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class CountriesAdapter implements CountryGateway {

    private final Countries countries;

    @Override
    public List<Country> getCountries() {
        return countries.findAll().stream().map(this::toCountry).collect(Collectors.toList());
    }

    private Country toCountry(CountryEntity countryEntity) {
        return new Country(countryEntity.id(), countryEntity.name());
    }
}
