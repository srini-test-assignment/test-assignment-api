dependencies {
    implementation(project(":common"))
    implementation(project(":application"))

    implementation("org.apache.commons:commons-lang3")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("javax.transaction:javax.transaction-api")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
}