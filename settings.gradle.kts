rootProject.name = "test-assignment-api"
include(
    "adapters:persistence",
    "adapters:web",
    "application",
    "common",
    "configuration",
)
